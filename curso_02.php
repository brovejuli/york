<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>York Trips</title>
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/icon" href="assets/images/favicon.ico"/>
    <!-- Font Awesome -->
    <link href="assets/css/font-awesome.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <!-- Slick slider -->
    <link rel="stylesheet" type="text/css" href="assets/css/slick.css"/>
    <!-- Fancybox slider -->
    <link rel="stylesheet" href="assets/css/jquery.fancybox.css" type="text/css" media="screen" />
    <!-- Animate css -->
    <link rel="stylesheet" type="text/css" href="assets/css/animate.css"/>
    <!-- Progress bar  -->
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap-progressbar-3.3.4.css"/>
    <!-- Theme color -->
    <link id="switcher" href="assets/css/theme-color/red-theme.css" rel="stylesheet">

    <!-- Main Style -->
    <link href="style.css" rel="stylesheet">

    <!-- Fonts -->

    <!-- Open Sans for body font -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <!-- Lato for Title -->
    <link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=News+Cycle" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<!-- BEGAIN PRELOADER -->
<div id="preloader">
    <div id="status">&nbsp;</div>
</div>
<!-- END PRELOADER -->

<!-- SCROLL TOP BUTTON -->
<a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
<!-- END SCROLL TOP BUTTON -->

<?php include("common/header.php"); ?>




<!-- Start login modal window -->
<div aria-hidden="false" role="dialog" tabindex="-1" id="login-form" class="modal leread-modal fade in">
    <div class="modal-dialog">
        <!-- Start login section -->
        <div id="login-content" class="modal-content">
            <div class="modal-header">
                <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title"><i class="fa fa-unlock-alt"></i>Login</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <input type="text" placeholder="User name" class="form-control">
                    </div>
                    <div class="form-group">
                        <input type="password" placeholder="Password" class="form-control">
                    </div>
                    <div class="loginbox">
                        <label><input type="checkbox"><span>Remember me</span></label>
                        <button class="btn signin-btn" type="button">SIGN IN</button>
                    </div>
                </form>
            </div>
            <div class="modal-footer footer-box">
                <a href="#">Forgot password ?</a>
                <span>No account ? <a id="signup-btn" href="#">Sign Up.</a></span>
            </div>
        </div>
        <!-- Start signup section -->
        <div id="signup-content" class="modal-content">
            <div class="modal-header">
                <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title"><i class="fa fa-lock"></i>Sign Up</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <input placeholder="Name" class="form-control">
                    </div>
                    <div class="form-group">
                        <input placeholder="Username" class="form-control">
                    </div>
                    <div class="form-group">
                        <input placeholder="Email" class="form-control">
                    </div>
                    <div class="form-group">
                        <input type="password" placeholder="Password" class="form-control">
                    </div>
                    <div class="signupbox">
                        <span>Already got account? <a id="login-btn" href="#">Sign In.</a></span>
                    </div>
                    <div class="loginbox">
                        <label><input type="checkbox"><span>Remember me</span><i class="fa"></i></label>
                        <button class="btn signin-btn" type="button">SIGN UP</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End login modal window -->



<!-- Start single page header -->
<section id="single-page-header">
    <div class="overlay">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">

                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="single-page-header-right">
                        <ol class="breadcrumb">
                            <li><img src="assets/images/viajar.png"></li>

                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End single page header -->

<!-- Start blog archive -->
<section id="blog-archive">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="title1 text-center">
                    <h1><span>Diciembre 2018 </span> I CURSO Y ESTADÍA EN BOURNEMOUTH</h1>
                    <br>
                    <p style="background-color:#323486; color: #ffffff; padding: 3px; margin-left: 470px; margin-right: 470px">El programa incluye:</p>
                    <br>
                </div>
                <div class="blog-archive-area">
                    <div class="row">
                        <div class="col-md-8">

                            <div class="blog-archive-left">
                                <!-- Start blog news single -->
                                <article class="blog-news-single">
                                    <div class="blog-news-title text-center">
                                    </div>
                                    <div class="col-md-6">
                                        <br><br><br>
                                    <p>Viví una experiencia cultural educativa única.<span> Pasá fin de año en Londres! </span>Estudia inglés y conocé las emblemáticas ciudades de Bournemouth, alojándote en casas de familia. </p>
                                    </div>
                                    <div class="col-md-6">
                                        <img class="img-responsive text-center" src="assets/images/londres_fin.png">
                                    </div>
                                    <div class="col-md-12 text-center">
                                        <img src="assets/images/banda.png">
                                    <br><br>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <div class="col-md-5">
                                            <img class="img-responsive" src="assets/images/curso_01a.png">
                                        </div>
                                        <div class="col-md-7">
                                            <h2>VUELO</h2>
                                            <p>Buenos Aires-Londres
                                               Londres-Buenos Aires
                                                <span>+ traslados</span>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <div class="col-md-5">
                                            <img class="img-responsive" src="assets/images/curso_01b.png">
                                        </div>
                                        <div class="col-md-7">
                                            <h2>HOSPEDAJE</h2>
                                            <p>
                                                Alojamiento en <span>casa de familia</span> y pensión completa en Bournemouth.
                                            </p>
                                        </div>
                                    </div>

                                    <div class="col-md-6 col-sm-6">
                                        <br><br>
                                        <div class="col-md-5">
                                            <img class="img-responsive" src="assets/images/curso_01c.png">
                                        </div>
                                        <div class="col-md-7">
                                            <h2>CURSOS</h2>
                                            <p>Matrícula de inscripción.
                                                Material de estudio.
                                                Test inicial de nivelación y <span>certificado</span> al finalizar.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <br><br>
                                        <div class="col-md-5">
                                            <img class="img-responsive" src="assets/images/curso_01d.png">
                                        </div>
                                        <div class="col-md-7">
                                            <h2>ACTIVIDADES</h2>
                                            <p>
                                                1 excursión de día completo por semana; 5 actividades sociales y deportivas por semana.
                                            </p>
                                            <br>
                                        </div>

                                    </div>
                                    <div class="col-md-offset-1 col-md-10 col-md-offset-1"><img class="img-responsive" src="assets/images/coordinadores.png"></div>

                                    <div>

                                    <img class="img-responsive" style="width: 97%;" src="assets/images/datos2.png">
                                    </div>
                                    <div><h5 style="float:right; padding-right: 20px">RESERVAS USD 200</h5></div>
                                    <div>
                                        <br><br>  <br>
                                        <p> Para mayor información, coordinar entrevista comunicándose a <span> (02223) 435817 - (02223) 442553 /</span> email <span>info@yorktrips.com.ar / </span> en Instituto York, Ferrari 261 / <a href="https://www.facebook.com/institutoyork.brandsen?fref=ts" target="_blank"><span><i class="fa fa-facebook" aria-hidden="true"></i> </span> Instituto York Brandsen.</a> </p>
                                        <br><br>
                                    </div>

                                </article>





                            </div>
                        </div>
                        <div class="col-md-4">
                            <aside class="blog-side-bar">
                                <!-- Start sidebar widget -->
                                <div class="sidebar-widget text-center">
                                    <img src="assets/images/anglo2.png">


                                </div>
                                <div>
                                    <p>Anglo Continental International School ofrece un excelente ambiente e instalaciones para aprender inglés, con un centro de aprendizaje multimedia, laboratorio de idiomas y libre acceso a internet. Eventos sociales y temáticos, actividades deportivas. Restaurant de autoservicio. Los centros de Anglo-Continental están ubicados en zonas residenciales tranquilas a poca distancia de la playa y del centro de Bournemouth.
                                    </p>
                                </div>
                                <div class="sidebar-widget text-center">

                                    <img src="assets/images/mapa.png">

                                </div>

                                <div class="sidebar-widget">
                                    <h4 class="widget-title">Tipo de cursos</h4>
                                    <p>Inglés general de 20 clases semanales. Test de nivelación previo y certificados de estudios al finalizar. Todos los niveles de inglés, desde principiantes a avanzado. Grupos con un máximo de 15 alumnos.</p>
                                </div>
                                <!-- Start sidebar widget -->
                                <div class="sidebar-widget">
                                    <h4 class="widget-title">Edades y programas:</h4>
                                    <ul class="widget-catg">
                                        <li><a href="#">Young Learners: de 13 a 15 años.</a></li>
                                        <li><a href="#">Vacation Programme: de 16 a 18 años.</a></li>
                                        <li><a href="#">Adult Programme: a partir de 19 años.</a></li>

                                    </ul>
                                </div>


                            </aside>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End blog archive -->






<img style="float:left" src="assets/images/rayas.png">


<?php include("common/footer.php"); ?>

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<!-- Bootstrap -->
<script src="assets/js/bootstrap.js"></script>
<!-- Slick Slider -->
<script type="text/javascript" src="assets/js/slick.js"></script>
<!-- mixit slider -->
<script type="text/javascript" src="assets/js/jquery.mixitup.js"></script>
<!-- Add fancyBox -->
<script type="text/javascript" src="assets/js/jquery.fancybox.pack.js"></script>
<!-- counter -->
<script src="assets/js/waypoints.js"></script>
<script src="assets/js/jquery.counterup.js"></script>
<!-- Wow animation -->
<script type="text/javascript" src="assets/js/wow.js"></script>
<!-- progress bar   -->
<script type="text/javascript" src="assets/js/bootstrap-progressbar.js"></script>


<!-- Custom js -->
<script type="text/javascript" src="assets/js/custom.js"></script>

</body>
</html>