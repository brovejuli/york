<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>York Trips</title>
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/icon" href="assets/images/favicon.ico"/>
    <!-- Font Awesome -->
    <link href="assets/css/font-awesome.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">    
    <!-- Slick slider -->
    <link rel="stylesheet" type="text/css" href="assets/css/slick.css"/> 
    <!-- Fancybox slider -->
    <link rel="stylesheet" href="assets/css/jquery.fancybox.css" type="text/css" media="screen" /> 
    <!-- Animate css -->
    <link rel="stylesheet" type="text/css" href="assets/css/animate.css"/> 
    <!-- Progress bar  -->
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap-progressbar-3.3.4.css"/> 
     <!-- Theme color -->
    <link id="switcher" href="assets/css/theme-color/red-theme.css" rel="stylesheet">

    <!-- Main Style -->
    <link href="style.css" rel="stylesheet">

    <!-- Fonts -->

    <!-- Open Sans for body font -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <!-- Lato for Title -->
    <link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>    
    
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  
  <!-- BEGAIN PRELOADER -->
  <div id="preloader">
    <div id="status">&nbsp;</div>
  </div>
  <!-- END PRELOADER -->

  <!-- SCROLL TOP BUTTON -->
  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->

  <?php include("common/header.php"); ?>





  <!-- Start slider -->
  <section id="slider">
    <div class="main-slider">
      <div class="single-slide">
        <img src="assets/images/slider-1.jpg" alt="img">
        <div class="slide-content">
          <div class="container">
            <div class="row">
              <div class="col-md-6 col-sm-6">
                <div class="slide-article">
                  <h1 class="wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.5s">Aprender viajando</h1>
                  <p class="wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.75s">Viajes culturales educativos. A medida y avalados por Anglo Continental. </p>
                </div>
              </div>
              <div class="col-md-6 col-sm-6">
                <div class="slider-img wow fadeInUp">
                  <img src="assets/images/avion.png" alt="business man">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="single-slide">
        <img src="assets/images/slider-3.jpg" alt="img">
        <div class="slide-content">
          <div class="container">
            <div class="row">
              <div class="col-md-6 col-sm-6">
                <div class="slide-article">
                  <h1 class="wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.5s">Una experiencia <br> cultural única</h1>
                  <p class="wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.75s">En casas de residentes de Bournemouth, especialmente seleccionadas. </p>
                </div>
              </div>
              <div class="col-md-6 col-sm-6">
                <div class="slider-img wow fadeInUp">
                  <img src="assets/images/casa.png" alt="business man">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="single-slide">
        <img src="assets/images/slider-5.jpg" alt="img">
        <div class="slide-content">
          <div class="container">
            <div class="row">
              <div class="col-md-6 col-sm-6">
                <div class="slide-article">
                  <h1 class="wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.5s">Especializaciones</h1>
                  <p class="wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.75s">Cursos intensivos para profesionales.</p>
                </div>
              </div>
              <div class="col-md-6 col-sm-6">
                <div class="slider-img wow fadeInUp">
                  <img src="assets/images/cursos.png" alt="business man">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-8"></div>
    <div class="wow bounceInRight col-md-4 anglo">
      <a href=""><img src="assets/images/anglo.png" alt="business man"></a>
    </div>

  </section>
  <!-- End slider -->

  <!-- Start Feature -->
  <section id="feature">
    <div class="container">
      <div class="row">

        <div class="col-md-12 iconos">
          <h3>Vos elegís como viajar </h3>
          <div class="feature-content">
            <div class="row">
              <div class="col-md-3 col-sm-6">
                <img src="assets/images/01a.png" onmouseover="this.src='assets/images/01b.png';" onmouseout="this.src='assets/images/01a.png';"/>

                <div class="single-feature wow zoomIn">
                  <h4 class="feat-title">Vuelo</h4>
                  <p>

                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Id leo blandit sociis! Urna ornare ut quis cubilia maecenas aliquet auctor. Facilisis congue ac fames sollicitudin ligula rhoncus vitae. </p>
                </div>
              </div>
              <div class="col-md-3 col-sm-6">
                <img src="assets/images/02a.png" onmouseover="this.src='assets/images/02b.png';" onmouseout="this.src='assets/images/02a.png';"/>

                <div class="single-feature wow zoomIn">
                  <h4 class="feat-title">Hospedaje</h4>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Id leo blandit sociis! Urna ornare ut quis cubilia maecenas aliquet auctor. Facilisis congue ac fames sollicitudin ligula rhoncus vitae.</p>
                </div>
              </div>
              <div class="col-md-3 col-sm-6">
                <img src="assets/images/03a.png" onmouseover="this.src='assets/images/03b.png';" onmouseout="this.src='assets/images/03a.png';"/>

                <div class="single-feature wow zoomIn">
                  <h4 class="feat-title">City Tours</h4>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Id leo blandit sociis! Urna ornare ut quis cubilia maecenas aliquet auctor. Facilisis congue ac fames sollicitudin ligula rhoncus vitae.</p>
                </div>
              </div>
              <div class="col-md-3 col-sm-6">
                <img src="assets/images/04b.png"/>

                <div class="single-feature wow zoomIn">
                  <h4 class="feat-title">Cursos</h4>
                  <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</p>
                </div>
              </div>


            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End Feature -->





  <img style="float:left" src="assets/images/rayas.png">


  <?php include("common/footer.php"); ?>

  <!-- jQuery library -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>    
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <!-- Bootstrap -->
  <script src="assets/js/bootstrap.js"></script>
  <!-- Slick Slider -->
  <script type="text/javascript" src="assets/js/slick.js"></script>    
  <!-- mixit slider -->
  <script type="text/javascript" src="assets/js/jquery.mixitup.js"></script>
  <!-- Add fancyBox -->        
  <script type="text/javascript" src="assets/js/jquery.fancybox.pack.js"></script>
 <!-- counter -->
  <script src="assets/js/waypoints.js"></script>
  <script src="assets/js/jquery.counterup.js"></script>
  <!-- Wow animation -->
  <script type="text/javascript" src="assets/js/wow.js"></script> 
  <!-- progress bar   -->
  <script type="text/javascript" src="assets/js/bootstrap-progressbar.js"></script>  
  
 
  <!-- Custom js -->
  <script type="text/javascript" src="assets/js/custom.js"></script>
  
  </body>
</html>